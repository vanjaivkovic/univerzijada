import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import HomeComponent from './HomeComponent';
import AboutComponent from './AboutComponent';
import TeamComponent from './TeamComponent';
import NotFoundComponent from './NotFoundComponent';

function App() {
  return (
    <Router>
      <div>
        <Switch>
            <Route exact path='/' component={HomeComponent}></Route>
            <Route exact path='/about' component={AboutComponent}></Route>
            <Route exact path='/team' component={TeamComponent}></Route>
            <Route component={NotFoundComponent}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
