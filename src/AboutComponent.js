import React from 'react';
import HeaderComponent from './HeaderComponent';

function AboutComponent() {
    return (
        <div>
            <HeaderComponent></HeaderComponent>
            <h1>About Component</h1>
        </div>
    )
}

export default AboutComponent;