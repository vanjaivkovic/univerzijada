import React from 'react';
import HeaderComponent from './HeaderComponent';

function HomeComponent() {
    return (
        <div>
            <HeaderComponent></HeaderComponent>
            <h1>Home Component</h1>
        </div>
    )
}

export default HomeComponent;