import React from 'react';
import HeaderComponent from './HeaderComponent';

function TeamComponent (){
    return (
        <div>
            <HeaderComponent></HeaderComponent>
            <h1>Team Component</h1>
        </div>
    )
}

export default TeamComponent;